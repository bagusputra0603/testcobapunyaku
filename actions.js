/*
 * action types
 */
// export const ADD_TODO = 'ADD_TODO'
// export const TOGGLE_TODO = 'TOGGLE_TODO'
// export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'
/*
 * other constants
 */

export const CHANGE_USERNAME = 'CHANGE_USERNAME'
export const CHANGE_UMUR = 'CHANGE_UMUR'
export const CHANGE_JOB = 'CHANGE_JOB'
export const CHANGE_ALAMAT ='CHANGE_ALAMAT'

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}
/*
 * action creators
 */
export function changeUsername(username) {
  console.log(username)
  return { type: CHANGE_USERNAME, username }
}
export function changeUmur(umur) {
  console.log(umur)
  return { type: CHANGE_UMUR, umur }
}
export function changeJob(job) {
  console.log(job)
  return { type: CHANGE_JOB, job }
}

export function changeAlamat(alamat) {
  console.log(alamat)
  return { type: CHANGE_ALAMAT, alamat }
}