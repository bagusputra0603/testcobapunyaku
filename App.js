/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import { Provider } from 'react-redux';
import Home from './Home';
import Move from './Move';
import Update from './Update';
import store from './store';
const MainNavigator = createStackNavigator({
  Home: {screen: Home,navigationOptions:{headerShown: false}},
  Move: {screen: Move,navigationOptions:{headerShown: false}} ,
  Update: {screen: Update,navigationOptions:{headerShown: false}} ,
});

const Root = createAppContainer(MainNavigator);

const App = () => (
  <Provider store={store}>
    <Root />
  </Provider>
)

export default App;

// AppRegistry.registerComponent('App', () => App)