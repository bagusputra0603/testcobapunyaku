import React, { Component } from 'react';
import { Text, View, SafeAreaView, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import { changeUsername,changeJob,changeUmur } from './actions';

class Move extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // text: '',
      // angka: '',
      // pertext: 'wkwkwkwkwkwkwk'

      username: '',
      umur: '',
      job: '',
      alamat: ''

    };
  }

  render() {

    // const { navigation } = this.props;
    // const get_text = navigation.getParam('sendtext', 'NO-User');
    // const get_angka = navigation.getParam('sendangka', 'kosong');
    // const get_pertext= navigation.getParam('sendpertext','kosong')
    // const get_username = navigation.getParam('sendusername', 'no name');
    // const get_umur = navigation.getParam('sendumur', 'no umur');
    // const get_job = navigation.getParam('sendjob', 'no job');
    return (
      <SafeAreaView>
        {/* <View >
          <Text>{JSON.stringify(get_text)}</Text>
          <Text>{JSON.stringify(get_angka)}</Text>
          <Text>{JSON.stringify(get_pertext)}</Text>
        </View> */}

        <View>

          <Text>{this.props.username}</Text>
          <Text>{this.props.umur}</Text>
          <Text>{this.props.job}</Text>
          <Text>{this.props.alamat}</Text>

          <Button
            title='Klik for Update'
            onPress={() =>
              this.props.navigation.navigate('Update', {
                usernamesend: this.state.username,
                umursend: this.state.umur,
                jobsend: this.state.job,
                alamtsend: this.state.alamat,
              })
            }
          />

        </View>

      </SafeAreaView>

    );
  }
}

const mapStateToProps = (state) => ({
  username: state.data.username,
  umur: state.data.umur,
  job: state.data.job,
  alamat:state.data.alamat
})



export default connect(mapStateToProps)(Move)
