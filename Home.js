import React, { Component } from 'react';
import { Text, View, Button, FlatList, TextInput, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { changeUsername, changeJob, changeUmur, changeAlamat } from './actions';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // text: '',
      // angka: '',
      // pertext: 'wkwkwkwkwkwkwk'

      username: '',
      umur: '',
      job: '',
      alamat: ''

    };
  }

  // loginbtn(){
  //   if(username){

  //   }
  // }

  render() {
    return (

      <SafeAreaView>
        <View>

          {/* <TextInput
            placeholder='Masukan Text'
            onChangeText={(text) => this.setState({ text })}
          />



          <TextInput
            placeholder='Masukan angka'
            onChangeText={(angka) => this.setState({ angka })}
          />



          <Button
            title='aaaaa'
            onPress={() =>
              this.props.navigation.navigate('Move', {
                sendtext: this.state.text,
                sendangka: this.state.angka,
                sendpertext: this.state.pertext
              })
            }
          /> */}

          <TextInput
            placeholder='Username'
            value={this.props.username}
            onChangeText={this.props.changeUsername}
          />


          <TextInput
            placeholder='Umur'
            value={this.props.umur}
            onChangeText={this.props.changeUmur}
          />

          <TextInput
            placeholder='Job'
            value={this.props.job}
            onChangeText={this.props.changeJob}
          />

          <TextInput
            placeholder='alamat'
            value={this.props.alamat}
            onChangeText={this.props.changeAlamat}
          />


          <Button
            title='Login'
            onPress={() =>
              this.props.navigation.navigate('Move', {
                sendusername: this.state.username,
                sendumur: this.state.umur,
                sendjob: this.state.job,
                sendalamat: this.state.alamat
              })
            }
          />



        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  username: state.data.username,
  umur: state.data.umur,
  job: state.data.job,
  alamat: state.data.alamat
})

const mapDispatchToProps = (dispatch) => ({
  changeUsername: (value) => dispatch(changeUsername(value)),
  changeUmur: (value) => dispatch(changeUmur(value)),
  changeJob: (value) => dispatch(changeJob(value)),
  changeAlamat: (value) => dispatch(changeAlamat(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
