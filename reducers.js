import { combineReducers } from 'redux'
import {
  ADD_TODO,
  TOGGLE_TODO,
  SET_VISIBILITY_FILTER,
  VisibilityFilters,
  CHANGE_USERNAME,
  CHANGE_UMUR,
  CHANGE_JOB,
  CHANGE_ALAMAT,
} from './actions'
const { SHOW_ALL } = VisibilityFilters
function visibilityFilter(state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter
    default:
      return state
  }
}

const initialState = {
  username: '',
  umur: '',
  job: '',
  alamat:''
}

function data(state = initialState, action) {
  console.log(action)
  switch (action.type) {
    
    case CHANGE_USERNAME:
      return {
        ...state,
        username: action.username
      }
    case CHANGE_UMUR:

      return {
        ...state,
        umur: action.umur
      }
    case CHANGE_JOB:

      return {
        ...state,
        job: action.job
      }

      case CHANGE_ALAMAT:

        return {
          ...state,
          alamat: action.alamat
        }

    default:
      return state
  }
}
const todoApp = combineReducers({
  visibilityFilter,
  data
})
export default todoApp